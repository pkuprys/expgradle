package edu.uchicago.cs.quiz.pkuprys.test;

import android.content.Context;

import edu.uchicago.cs.quiz.pkuprys.R;

/**
 * Created by ag on 8/14/13.
 */
public class Watch {

    public static final int TIME_DELAY = 0;

    public static void watch(){
        delay(TIME_DELAY);
    }

    public static void delay(int nDelay){

        try {
            Thread.sleep(nDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
